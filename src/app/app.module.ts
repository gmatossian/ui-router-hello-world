import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { UIRouterModule } from '@uirouter/angular';
import { AppComponent } from './app.component';
import { AboutComponent } from './about/about.component';
import { HelloComponent } from './hello/hello.component';

const STATES = [
  { name: 'hello', url: '/hello',  component: HelloComponent },
  { name: 'about', url: '/about',  component: AboutComponent }
];

@NgModule({
  declarations: [
    AboutComponent,
    AppComponent,
    HelloComponent
  ],
  imports: [
    BrowserModule,
    UIRouterModule.forRoot({
      states: STATES
    })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
